#Seeds mongo
require 'csv'

#Drop user collection before load data from .cvs
User.collection.drop

path = "#{Rails.root}/db/json/"
json_array = []
Dir.open(path).each do |f|
  if f.match(/(.*)\.json$/)
    json_array = JSON.parse(File.read("#{path}#{$1}.json"))
    #Country.create(records)
  end
end
#Get the full path where is the .csv file to load
path = "#{Rails.root}/db/csvs/"
#Go to the directory
Dir.open(path).each do |f|
  # if any file match with .csv. will each csv using CSV class.
  if f.match(/(.*)\.csv$/)
    CSV.foreach("#{path}#{$1}.csv", headers: true, header_converters: :symbol) do |row|
      ioc_code = json_array.find { |element| element['cioc'] == row[:nationality]}
      User.create(id: row[:id], name: row[:name], nationality: row[:nationality], sex: row[:sex], date_of_birth: row[:date_of_birth], a3: ioc_code['alpha3Code'], country_name: ioc_code['name']) if !ioc_code.nil?
      User.create(id: row[:id], name: row[:name], nationality: row[:nationality], sex: row[:sex], date_of_birth: row[:date_of_birth], a3: row[:nationality], country_name: row[:nationality]) if ioc_code.nil?
    end
  end
end

