Rails.application.routes.draw do
  scope '/api' do
    get 'users', to: 'user#all', as: 'user_all'
    post 'users', to: 'user#all_filter', as: 'user_all_filter'
  end
end
