class User
  include Mongoid::Document
  field :id, type: String
  field :name, type: String
  field :nationality, type: String
  field :sex, type: String
  field :date_of_birth, type: String
  field :a3, type: String
  field :country_name, type: String

  # Filter users.
  # by gender, by age, and grouped by nationality
  # Retrieve filtered users
  def self.filters(data)
    m_year = {}
    m_gender = {}
    group = {"$group": { "_id": {"nationality": "$nationality", "a3": "$a3", "country_name": "$country_name"}, "count": {"$sum":  1}}}
    m_gender = {"$match": {"sex": "#{data[:gender]}" }} if data[:gender] != 'both'
    if data[:more_filters] && data[:filter_age] != ''
      year = Date.today.year - data[:filter_age]
      year_second = Date.today.year - data[:filter_age_second]
      m_year = {"$match": {"$and": [{"date_of_birth": {"$gte": "#{year_second}-01-01"}}, {"date_of_birth": {"$lte": "#{year}-12-31"}}]}}
    elsif data[:filter_age] != ''
      year = Date.today.year - data[:filter_age]
      m_year = {"$match": {"$and": [{"date_of_birth": {"$gte": "#{year}-01-01"}}, {"date_of_birth": {"$lte": "#{year}-12-31"}}]}}
    end
    users = User.collection.aggregate [m_year, m_gender, group] if data[:gender] != 'both' && data[:filter_age] != ''
    users = User.collection.aggregate [m_gender, group] if data[:gender] != 'both' && data[:filter_age] == ''
    users = User.collection.aggregate [m_year, group] if data[:gender] == 'both' && data[:filter_age] != ''
    users = User.collection.aggregate [group] if data[:gender] == 'both' && data[:filter_age] == ''
    users
  end
end
