class UserController < ApplicationController
  skip_before_action :verify_authenticity_token

  # Retrieve json wti all users grouped by nationality
  def all
    users = User.collection.aggregate [{"$group": { "_id": {"nationality": "$nationality", "a3": "$a3", "country_name": "$country_name"}, "count": {"$sum":  1} }}]
    render json: { users: users }
  rescue StandardError => e
    render json: { message: "Bad Request: #{e.message}" }, status: 400
  end
  # User's filter
  # params [data], gender: String, age: Integer, age_second: Integer, more_filters: Boolean 
  def all_filter
    if params[:data]
      users = User.filters(params[:data])
      render json: { users: users }
    else
      render json: { message: 'No params found' }, status: 402
    end
  rescue StandardError => e
    render json: { message: "Bad Request: #{e.message}" }, status: 400
  end
end
